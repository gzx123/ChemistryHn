﻿using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chemistry.Pages
{
    /// <summary>
    /// ExamingStatePage.xaml 的交互逻辑
    /// </summary>
    public partial class ExamingStatePage : UserControl
    {
        public ExamingStatePage()
        {
            InitializeComponent();
        }

        private void PgChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnleak_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TgChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
           // this.Hide();
            Messenger.Default.Send<object>("", MessageToken.HidenExamingWindow);
        }
    }
}
