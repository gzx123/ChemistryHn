﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chemistry.Pages
{
    using System.Collections.ObjectModel;

    using Chemistry.Models;

    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// RegulationButtonPage.xaml 的交互逻辑
    /// </summary>
    public partial class RegulationButtonPage : UserControl
    {
        public RegulationButtonPage()
        {
            InitializeComponent();            
        }

        private void FrameworkElement_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            //var buttons = datas.DataContext as List<ButtonInformation>;
        }

        private void RangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var buttons = (datas.ItemsSource as IEnumerable<ButtonInformation>).ToList();
            Messenger.Default.Send<List<ButtonInformation>>(buttons, MessageToken.SendButtonValue);
        }
    }
}
