﻿
using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;

namespace Chemistry.Pages
{
    /// <summary>
    /// InputNumberPage.xaml 的交互逻辑
    /// </summary>
    public partial class InputNumberPage : Flyout
    {
        public InputNumberPage()
        {
            InitializeComponent();
            
        }

        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                Messenger.Default.Send<string>(TxtTicketid.Text.Trim(), MessageToken.InputNumberOver);
            }
        }
    }
}
