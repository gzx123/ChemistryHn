﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Chemistry.Models;

namespace Chemistry.Pages
{
    /// <summary>
    /// CheckDetailPage.xaml 的交互逻辑
    /// </summary>
    public partial class CheckDetailPage : Flyout
    {
        public CheckDetailPage()
        {
            InitializeComponent();

            Messenger.Default.Register<string>(this,MessageToken.CheckDetail, (i) => {
                rtb.Document.Blocks.Clear();
                this.IsOpen = true;
                Paragraph p = new Paragraph();
                Run r = new Run(i);
                p.Inlines.Add(r);
                rtb.Document.Blocks.Add(p);
            });
        }
    }
}
