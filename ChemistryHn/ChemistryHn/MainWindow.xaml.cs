﻿using Chemistry.Models;
using Chemistry.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chemistry
{
    using Chemistry.Views.ExamViews;
    using System.Configuration;
    using System.Text.RegularExpressions;
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : MahApps.Metro.Controls.MetroWindow
    { 
        public MainWindow()
        { 
            InitializeComponent();
            InitButtons();
            Closing += MainWindow_Closing;
            InitConfigButton();

            InitVersion();
        }

        private void InitVersion()
        {
            var value = Tools.Tool.GetConfigValue("UpDateTime");
            var version = int.Parse(Regex.Replace(value, @"[^\d]", ""));
            txtVersoin.Text = "v" + version;
        }

        private void InitConfigButton()
        {
            btnK3exam.Visibility = System.Windows.Visibility.Collapsed;
            btnExampoint.Visibility = System.Windows.Visibility.Collapsed;
            btnFlowConfig.Visibility = System.Windows.Visibility.Collapsed;

        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void InitButtons()
        {
            List<Tools.Tool.ButtonConfig> buttons = Tools.Tool.GetButtons();

            foreach (var item in buttons)
            {
                if (item.IsShow)
                {
                    Button button = new Button();
                    button.Content = item.Name;
                    button.Tag = item;
                    button.Style = Resources["ButtonStyle1"] as Style;
                    button.Margin = new Thickness(5);
                    button.Click += button_Click;

                    if (item.Name.Contains("内操"))
                        leftpanel.Children.Add(button);
                    else
                        rightpanel.Children.Add(button);
                }

            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Tools.Tool.ButtonConfig buttonconfig = button.Tag as Tools.Tool.ButtonConfig;
            SetExamModel(buttonconfig);
            ShowExamWindow();
        }

        private void ShowExamWindow()
        {
            ExamWindow win =new  ExamWindow();
            win.MainWindow = this;
            win.Show();
            this.Hide();
        }

        private static void SetExamModel(Tools.Tool.ButtonConfig config)
        {
            ConfigModel model = Tools.Tool.GetConfig();
            model.IsInner = config.IsInner;
            model.WorkflowClassName = config.ClassName;
            model.WorkflowChineseName = config.WorkflowChineseName;
            using (FileStream fs = new FileStream(Global.CONFIGDATA_PATH, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, model);
            }
        }

        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            ConfigWindow win = new ConfigWindow();
            win.Show();
        }

        private void btnConfigScore_Click(object sender, RoutedEventArgs e)
        {
            ConfigScoreWindow win = new ConfigScoreWindow();
            win.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnExamPoint_Click(object sender, RoutedEventArgs e)
        {
            ConfigExaminationPoint win = new ConfigExaminationPoint();
            win.Show();
        }

        private void btnK3_Click(object sender, RoutedEventArgs e)
        {
            InputNumberWindow win = new InputNumberWindow();
            win.MainWindow = this;
            win.Show();
            this.Hide();
        }
    }
}
