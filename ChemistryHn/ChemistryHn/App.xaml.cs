﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Chemistry.Tools;
using GLCommon;

namespace Chemistry
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            LogTool.Init();
            InitHttpTool();
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            CheckUpdate();
        }
        private void CheckUpdate()
        {
            string version = String.Empty;
            if (Tool.CanUpdate(ref version))
            {
                try
                {
                    string path = AppDomain.CurrentDomain.BaseDirectory;
                    Process process = new Process
                    {
                        StartInfo =
                        {
                            FileName = AppDomain.CurrentDomain.BaseDirectory + "\\AutoUpdate.exe",
                            WorkingDirectory = path,
                            CreateNoWindow = true
                        }
                    };
                    process.Start();
                    CutProcess();

                }
                catch (Exception ex)
                {
                    // ignored
                }
            }
        }

        private static void CutProcess()
        {
            Process[] processes = Process.GetProcessesByName(Global.CompareFile);
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }
        private void InitHttpTool()
        {
            var configs = ConfigTool.GetConfig();
            var serverAddress = configs.SingleOrDefault(i => i.name == "ServerAddress");
            if (serverAddress != null)
            {
                var address = serverAddress.value;
                HttpTool.Client.BaseAddress = new Uri(address);
            }
        }

        private async void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Console.WriteLine(e.Exception);
            e.Handled = true;

            LogTool.UpServerLog(e.Exception.StackTrace, EventType.Error);

        }

        private async void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Exception exception = e.ExceptionObject as Exception;
            Console.WriteLine(exception.StackTrace);
            LogTool.UpServerLog(exception.StackTrace, EventType.Error);

        }
    }
}
