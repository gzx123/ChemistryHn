﻿using Chemistry.Models;
using Chemistry.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLCommon;

namespace Chemistry.WorkFlow
{
    public class FlowBaseAll:WorkFlowBase
    {
        #region 字段
        protected Dictionary<string, int> _compose;
        protected Dictionary<string, string> _checkdic;
        protected List<Detail> _details;
        //protected List<Abnormal> _abnormals;
        protected bool _isvetodown = false;//否决

        #endregion


        #region 公共方法
        public FlowBaseAll()
        {
            _compose = new Dictionary<string, int>();
            _checkdic = new Dictionary<string, string>();
            _details = new List<Detail>();
            //_abnormals = new List<Abnormal>();
            _isleakAbnormal = false;
            _ispressureAbnormal = false;
            _istemperatureAbnormal = false;
        }

        #endregion

        #region Protected virtual Method       
        protected override int CalcScore()
        {
            if (_isvetodown) //否决
            {

                return 0;
            }
            StringBuilder builder = new StringBuilder();
            int score = 0;

            // 异常计分
            if (_warningDicionary.Count > 0)
            {
                //处置了异常
                if (_warningDicionary.First().Value)
                {
                    score = this.GetScore(builder);
                    if (score > 100)
                    {
                        score = 100;
                    }
                    if (score < 60)
                    {
                        score = 60;
                    }
                }
                else
                {
                    score = this.GetScore(builder, true);
                    
                }
            }
            else
            {
                score = this.GetScore(builder, true);
            }
            this.Content = builder.ToString();
            LogTool.UpServerLog(Content, EventType.Information);
            
            return score;
        }

        /// <summary>
        /// 根据步骤 获取分数
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="useZero">是否用0代替负值</param>
        /// <returns></returns>
        private int GetScore(StringBuilder builder, bool useZero = false)
        {
            int score = 0;
            int index = 1;
            int nagetiveScore = 0;//负值分
            foreach (var i in this._compose.Keys)
            {
                var stepScore = _compose[i];
                string msg = String.Empty;
                if (stepScore < 0)
                {
                    if (useZero)
                    {
                        msg = string.Format("{0}. {1}\t分值：{2}", index, i, 0);
                    }
                    else
                    {
                        msg = string.Format("{0}. {1}\t分值：{2}", index, i, stepScore);
                        nagetiveScore += stepScore;
                    }
                }
                else // 正值
                {
                    msg = string.Format("{0}. {1}\t分值：{2}", index, i, stepScore);
                    score += stepScore;
                }

             
                Console.WriteLine(msg);

                builder.AppendLine(msg);
                index++;
            }
            if (!useZero)
            {
                if (score > 100) score = 100;
                score = score + nagetiveScore;
            }   
            return score;
        }

        #endregion

        #region 通用方法
        /// <summary>
        /// 根据键值对判断是否有异常
        /// </summary>
        /// <param name="value"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected bool Isabnormal(double value, string key)
        {
            var result = false;
            //var abnormal = _abnormals.SingleOrDefault(i => i.Description == key);
            //if (abnormal == null) return result;
            //double sub_value = value - abnormal.Value;
            //switch (abnormal.Operator)
            //{
            //    case Operator.大于:
            //        result = sub_value > 0;
            //        break;
            //    case Operator.小于:
            //        result = sub_value < 0;
            //        break;
            //}
            return result;
        }

        protected void AddCompose(string key)
        {
            if (!_compose.ContainsKey(key))
            {
                var score = _details.Where(i => i.Step == key).Select(i => i.Score).SingleOrDefault();
                _compose.Add(key, score);
            }
            else
            {
                var score = _details.Where(i => i.Step == key).Select(i => i.Score).SingleOrDefault();
                _compose[key] = score;
            }
        }
        protected void RemoveCompose(string key)
        {
            if (_compose.ContainsKey(key))
            {
                _compose.Remove(key);
            }
             
        }
        #endregion

        #region webapi

        protected async Task<List<Detail>> GetDetailList(string classname)
        {
            List<Detail> list = new List<Detail>();
            if (Global.NetMode == NetModel.Stand)
            {
                var workflowId = DbEntryClassLibrary.Models.WorkFlow.FindOne(i => i.ClassName == "Hydrogenation").Id;
                var details = from d in DbEntryClassLibrary.Models.Detail.Table
                    where d.WorkFlowId == workflowId
                    select d;
                var result=  details.ToList();
                list = result.Select(detail => new Detail() {Id = (int) detail.Id, Score = detail.Score, Step = detail.Step}).ToList();
            }
            else
            {
                string request_url = string.Format("{0}/GetDetailsByClassName?className={1}", "/Api/WorkFlowApi", classname);
                var reuslt = await HttpBaseChemistry.GetAsync<DetaiApiModel>(request_url);
                if (reuslt != null && reuslt.status == 0)
                    list = reuslt.details;
            }
            return list;
        }


        protected async Task<List<Abnormal>> GetAbnormals(string classname)
        {
            string request_url = string.Format("{0}/GetAbnormalsByClassName?classname={1}", "/Api/WorkFlowApi", classname);
            return await HttpBaseChemistry.GetAsync<List<Models.Abnormal>>(request_url);            
        }

        protected async Task<AbnormalApiModel> GetAbnormalModel(string classname)
        {
            string request_url = string.Format("{0}/GetAbnormalsByClassName?classname={1}", "/Api/WorkFlowApi", classname);
            return await HttpBaseChemistry.GetAsync<AbnormalApiModel>(request_url);
        }
        #endregion


        //开：  128|x
        //关：  ~128&x
    }
}
