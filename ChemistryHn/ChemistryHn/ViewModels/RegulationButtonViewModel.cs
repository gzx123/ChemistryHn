﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.ViewModels
{
    using System.Collections.ObjectModel;

    using Chemistry.Models;

    /// <summary>
    /// 调节按钮vm
    /// </summary>
    public class RegulationButtonViewModel : VmBase
    {
        private ObservableCollection<ButtonInformation> _buttons;
        public ObservableCollection<ButtonInformation> Buttons
        {
            get { return _buttons; }
            set { _buttons = value; RaisePropertyChanged("Buttons"); }
        }



        public RegulationButtonViewModel()
        {
            InitMessage();
        }

        private void InitMessage()
        {
            Register<ExamInfoMessage>(this, MessageToken.ShowExamingWindow,
                (i) =>
                {
                    if(i.ButtonInformations!=null)
                        Buttons = new ObservableCollection<ButtonInformation>(i.ButtonInformations);

                });
        }
    }
 
}
