﻿using Chemistry.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using Chemistry.Tools;
using System.Windows;
using GLCommon;

namespace Chemistry.ViewModels
{
    public class VmBase:ViewModelBase
    {
        protected string workflow_url = "/Api/WorkFlowApi";
        public RelayCommand CommitCommand { get; set; }
        protected MetroDialogSettings _metroDialogSettings;
        public VmBase()
        {
            _metroDialogSettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "确定",
                NegativeButtonText = "取消"
            };      
        }

        private bool _isOpen;
        public bool IsOpen
        {
            get { return _isOpen; }
            set { _isOpen = value; RaisePropertyChanged("IsOpen"); }
        }

        protected void Send<T>(T message, MessageToken token)
        {
            Messenger.Default.Send<T>(message, token);
        }

        protected void Send(MessageToken token)
        {
            Messenger.Default.Send<object>(null, token);
        }

        protected void Register<T>(object recipient, MessageToken token, Action<T> action)
        {
            Messenger.Default.Register<T>(recipient, token, action);
        }

        protected void Register(object recipient, MessageToken token, Action<object> action)
        {
            Messenger.Default.Register<object>(recipient, token, action);
        }

        protected void ShowMessage(string message)
        {
            ((MetroWindow)Application.Current.MainWindow).ShowMessageAsync("提示", message);
        }

        #region Async Method
        
        protected void AppendExecptionAsync(Exception ex)
        {
            LogTool.UpServerExceptionLog(ex);
        } 
        #endregion
    }
}
