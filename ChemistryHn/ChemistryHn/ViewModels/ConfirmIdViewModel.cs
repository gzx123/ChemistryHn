﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chemistry.Models;
using Chemistry.WorkFlow;
using GalaSoft.MvvmLight.Command;

namespace Chemistry.ViewModels
{
    public class ConfirmIdViewModel:VmBase
    {
        private Examinee _currentExaminee;
        private ImageSource _bursh;

        public ImageSource ImageSource
        {
            get { return _bursh; }
            set { _bursh = value; RaisePropertyChanged("ImageSource"); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChanged("Name"); }
        }

        private string _tickid;

        public string TickId
        {
            get { return _tickid; }
            set { _tickid = value; RaisePropertyChanged("TickId"); }
        }

        public RelayCommand BackCommand { get; set; }
        public ConfirmIdViewModel()
        {
            CommitCommand = new RelayCommand(Execute);
            BackCommand = new RelayCommand(BackExec);
            Register<Examinee>(this, MessageToken.ShowConfirId, (i) =>
            {
                _currentExaminee = i;
                InitExamineeInfo(i);
                IsOpen = true;
            });
        }

       

        private void InitExamineeInfo(Examinee examinee)
        {
            this.Name = examinee.Name;
            this.TickId = examinee.TicketId;
            if (examinee.Pictrue != null)
            {
                ImageBrush brush = new ImageBrush();
                ImageSourceConverter converter = new ImageSourceConverter();
                this.ImageSource = (ImageSource)converter.ConvertFrom(examinee.Pictrue);
            }
            else
            {
                var filename = "nophoto.jpg";
                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    int len = (int)fs.Length;
                    byte[] b = new byte[len];
                    fs.Read(b, 0, len);
                    examinee.Pictrue = b;
                }
            }
        }

        private void Execute()
        {
            IsOpen = false;

            Send<Examinee>(_currentExaminee, MessageToken.BeginExaming);   
        }
        private void BackExec()
        {
            IsOpen = false;
            Send(MessageToken.ShowInputNumber);
        }
    }
}
