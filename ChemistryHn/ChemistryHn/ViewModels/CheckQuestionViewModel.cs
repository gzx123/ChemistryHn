﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.ViewModels
{
    using System.Collections.ObjectModel;

    using Chemistry.Models;

    public class CheckQuestionViewModel:VmBase
    {
        private ObservableCollection<ExaminationPoint> _points;
        public ObservableCollection<ExaminationPoint> Points
        {
            get { return _points; }
            set { _points = value; RaisePropertyChanged("Points"); }
        }

        public CheckQuestionViewModel()
        {
            InitMessage();
        }

        private void InitMessage()
        {
            Register<List<ExaminationPoint>>(
               this,
               MessageToken.CheckQuestion,
               (i) =>
               {
                   Console.WriteLine("这里执行了吗");
                   this.Points = new ObservableCollection<ExaminationPoint>(i);
               });
        }
    }
}
