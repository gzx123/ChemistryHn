﻿using System;
using System.Threading.Tasks;
using Chemistry.Models;
using Chemistry.Tools;
using GLCommon;

namespace Chemistry.ViewModels
{
    /// <summary>
    /// 输入准考证号vm
    /// </summary>
    public class InputNumberViewModel:VmBase
    {

        private bool _isButtonEnabled = true;
        public bool ButtonEnabled
        {
            get { return _isButtonEnabled; }
            set { _isButtonEnabled = value; RaisePropertyChanged("ButtonEnabled"); }
        }


        private string _number;
        /// <summary>
        /// 准考证号
        /// </summary>
        public string Number
        {
            get { return _number; }
            set { _number = value; RaisePropertyChanged("Number"); }
        }

        

        public InputNumberViewModel()
        {
            CommitCommand = new GalaSoft.MvvmLight.Command.RelayCommand(CommitExec,
                () => !string.IsNullOrEmpty(Number)&&_isButtonEnabled);

            Register(this, MessageToken.ShowInputNumber, (i) =>
            {
                _isButtonEnabled = true;
                this.Number = string.Empty;
                IsOpen = true;
            });

            Register<string>(this, MessageToken.InputNumberOver,   (i) =>
            {
                    this.Number = i;
                    CommitExec();
            });
        }

        private async void CommitExec()
        {
            _isButtonEnabled = false;
            await Execute();
            _isButtonEnabled = true;
        }

        private async Task Execute()
        {
            Examinee examinee = await GetExaminee();
            if (examinee != null)
            {
                examinee.TicketId = Number;
                Send<Examinee>(examinee, MessageToken.ShowConfirId);
            }
           
        }

        private async Task<Examinee> GetExaminee()
        {
            Send<string>("获取考试信息中......", MessageToken.ShowExamLoadMessage);
            Examinee examinee = null;
            string getexamineeUrl = string.Format("GetExaminee?nr={0}&session={1}", arg0: Number, arg1: HttpTool.Session);
            var result = await HttpTool.GetAsync<ExamineeInfoApi>(getexamineeUrl);
            await Task.Delay(1000);
            if (result.status == 0 && result.examinee != null)
            {
                if (!result.subject.Contains("hg"))
                {
                    Send<string>("获取考生信息中......", MessageToken.CloseExamLoadMessage);
                    await Task.Delay(1000);
                    Send<string>("该考生已完成考试，不能重复考试", MessageToken.ShowExamMessage);
                }
                else
                {
                    Send<string>("获取考生信息中......", MessageToken.CloseExamLoadMessage);
                    examinee = new Examinee { Name = result.examinee.name };

                    string info = string.Format("考生{0}登录", examinee.Name);
                   LogTool.WriteInfoLog(info);
                    RandomQeustion(examinee);
                    if (!string.IsNullOrEmpty(result.examinee.photo))
                        examinee.Pictrue = Convert.FromBase64String(result.examinee.photo);

                    IsOpen = false;
                }
            }
            else
            {
                Send<string>("获取考试信息中......", MessageToken.CloseExamLoadMessage);
                Send<string>("未找到考生信息", MessageToken.ShowExamMessage);
               
            }
            return examinee;
        }

        private static void RandomQeustion(Examinee examinee)
        {
            if (Global.Examinees.Count > 10)
                Global.Examinees.Clear();
            if (Global.Examinees.Count == 0)
                Global.Random = new Random().Next(10);

            Global.Examinees.Add(examinee);

            if (Global.Examinees.Count - 1 == Global.Random)
            {
                var random = new Random().Next(3);
                switch (random)
                {
                    case 0:
                        examinee.IsLeakAbnormal = true;
                        break;
                    case 1:
                        examinee.IsPressureAbnormal = true;
                        break;
                    case 2:
                        examinee.IsTemperatureAbnormal = true;
                        break;
                }
            }

        }
    }
}
