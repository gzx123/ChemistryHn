﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<EquipmentViewModel>();
            SimpleIoc.Default.Register<ConfigViewModel>();
            SimpleIoc.Default.Register<ConfigScoreViewModel>();
            SimpleIoc.Default.Register<ConfigExaminationPointViewModel>();
            SimpleIoc.Default.Register<SelectQuetionViewModel>();
            SimpleIoc.Default.Register<ConfirmIdViewModel>();
            SimpleIoc.Default.Register<InputNumberViewModel>();
            SimpleIoc.Default.Register<ExamingViewModel>();
            SimpleIoc.Default.Register<RegulationButtonViewModel>();
            SimpleIoc.Default.Register<K3InputNumberViewModel>();
            SimpleIoc.Default.Register<K3QuestionViewModel>();
            SimpleIoc.Default.Register<CheckQuestionViewModel>();
            SimpleIoc.Default.Register<DirectExamingViewModel>();
            K3InputNumber.ToString();
            K3Question.ToString();
            CheckQuestion.ToString();
            DirectExaming.ToString();
        }


        public EquipmentViewModel Equipment
        {
            get
            {
                //return SimpleIoc.Default.GetInstanceWithoutCaching<EquipmentViewModel>();
                return ServiceLocator.Current.GetInstance<EquipmentViewModel>();
            }
        }
        public ConfigViewModel Config
        {
            get { return ServiceLocator.Current.GetInstance<ConfigViewModel>(); }
        }
        public ConfigScoreViewModel ConfigScore
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ConfigScoreViewModel>();
            }
        }
        public ConfigExaminationPointViewModel ConfigExaminationPoint
        {
            get
            {
                return SimpleIoc.Default.GetInstanceWithoutCaching<ConfigExaminationPointViewModel>();
                //return ServiceLocator.Current.GetInstance<ConfigExaminationPointViewModel>();
            }
        }
        public SelectQuetionViewModel SelectQuetion
        {
           // get { return SimpleIoc.Default.GetInstanceWithoutCaching<SelectQuetionViewModel>(); }
            get { return ServiceLocator.Current.GetInstance<SelectQuetionViewModel>(); }
        }

        public ConfirmIdViewModel ConfirmId
        {
            get { return ServiceLocator.Current.GetInstance<ConfirmIdViewModel>(); }
        }

        public DirectExamingViewModel DirectExaming
        {
            get { return ServiceLocator.Current.GetInstance<DirectExamingViewModel>(); }
        }

        public InputNumberViewModel InputNumber
        {
            get { return ServiceLocator.Current.GetInstance<InputNumberViewModel>(); }
        }
        
        public ExamingViewModel Examing
        {
            get { return ServiceLocator.Current.GetInstance<ExamingViewModel>(); }
        }

        public RegulationButtonViewModel RegulationButton
        {
            get { return ServiceLocator.Current.GetInstance<RegulationButtonViewModel>(); }
        }

        public K3InputNumberViewModel K3InputNumber
        {
            get { return ServiceLocator.Current.GetInstance<K3InputNumberViewModel>(); }
        }

        public K3QuestionViewModel K3Question
        {
            get { return ServiceLocator.Current.GetInstance<K3QuestionViewModel>(); }
        }

        public CheckQuestionViewModel CheckQuestion
        {
            get { return ServiceLocator.Current.GetInstance<CheckQuestionViewModel>(); }
        }
    }
}



