﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chemistry.Models;
using MahApps.Metro.Controls.Dialogs;

namespace Chemistry.Views
{
    /// <summary>
    /// ExamWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ExamWindow : MahApps.Metro.Controls.MetroWindow
    {
        public MainWindow MainWindow { get; set; }

        private ExamingWindow _examingWindow;
        private DirectExamingWindow _directExamingWindow;
        private RegulationWindow _regulationWindow;
        private bool _isExaming;
        private MetroDialogSettings _settings;
        public ExamWindow()
        {

            InitializeComponent();
            Application.Current.MainWindow = this;
            
            InitTitle();
            InitMessage();

            this.Unloaded += ExamWindow_Unloaded;
        }

        void ExamWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (_isExaming)
            {
                var result = MessageBox.Show("检测到正在考试中，确认要退出吗？如果退出，考试将终止！", "提示", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                {
                    if (result != MessageBoxResult.OK) e.Cancel = true;
                    else
                    {
                        Messenger.Default.Send<object>(null,MessageToken.StopExaming);
                        base.OnClosing(e);
                    }
                        
                }
            }
            else
            {
                base.OnClosing(e);
            }
            _settings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "确定",
                NegativeButtonText = "取消",
                 
            };
            //if(_isExaming)
            //{
            //    var _setting = new MetroDialogSettings()
            //    {
            //        AffirmativeButtonText = "确定",
            //        NegativeButtonText = "取消"
            //    };
            //   var r =await  this.ShowMessageAsync("提示", "检测到正在考试中，确认要退出吗？如果退出，考试将终止！", MessageDialogStyle.AffirmativeAndNegative,_setting);
            //   if (r != MessageDialogResult.Affirmative)
            //   {
            //       e.Cancel = true;
            //   }

            //}
            //else
            //{
            //    base.OnClosing(e);
            //}
        }
        ProgressDialogController controller ;
        private void InitMessage()
        {
            
            Messenger.Default.Send<object>(null, MessageToken.ShowExamWindow);


            Messenger.Default.Register<string>(this, MessageToken.ShowExamMessage, async (i) =>
            {
                Console.WriteLine();
                await this.ShowMessageAsync("提示", i , MessageDialogStyle.Affirmative,_settings);
                
                //var controller1 = await this.ShowProgressAsync("提示", i);
                //controller1.SetIndeterminate();
                //controller = controller1;
            });

            Messenger.Default.Register<ExamInfoMessage>(this, MessageToken.ShowExamingWindow, (i) =>
            {
                //考试状态窗口
                _examingWindow = new ExamingWindow();
                _examingWindow.Show();

                Application.Current.MainWindow = _examingWindow;
                this.Hide();
            });

            Messenger.Default.Register<ExamInfoMessage>(this, MessageToken.ShowDirectExamingWindow, (i) =>
            {
                //直接进入的考试状态窗口
                _directExamingWindow = new DirectExamingWindow() ;
                _directExamingWindow.Show();

                Application.Current.MainWindow = _directExamingWindow;
                this.Hide();
            });

            Messenger.Default.Register<object>(this, MessageToken.HidenExamingWindow, (i) => {
                Application.Current.MainWindow = MainWindow;
                this.Show();
            });
            Messenger.Default.Register<object>(this, MessageToken.HidenDirectExamingWindow, (i) =>
            {
                Application.Current.MainWindow = MainWindow;
                this.Show();
            });

            Messenger.Default.Register<bool>(this, MessageToken.IsExaming, (i) => {
                _isExaming = i;
            });

           
            Messenger.Default.Register<string>(this, MessageToken.ShowExamLoadMessage, async (i) =>
                {

                  //  MessageBox.Show(i, "提示");
                    
                    controller = await this.ShowProgressAsync("提示", i);
                    controller.SetIndeterminate(); 
                    
                //controller = controller1;
            });
            Messenger.Default.Register<string>(this, MessageToken.CloseExamLoadMessage, async (i) =>
                {
                    if (controller!=null )
                    {
                        await controller.CloseAsync();
                    }
              
            });
        }

        private ConfigModel _config;
        private void InitTitle()
        {
            _config = Tools.Tool.GetConfig();
            string opname = _config.IsInner ? "内操考核" : "外操考核";
            string title = string.Format("危化考试 - {0} -[{1}]", _config.WorkflowChineseName, opname);
            this.Title = title;
        }

        private void ExamWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_examingWindow != null) this._examingWindow.Close();
            
            this.MainWindow.Show();

            Messenger.Default.Send<bool>(false, MessageToken.IsExaming);
            
        }

    }
}
