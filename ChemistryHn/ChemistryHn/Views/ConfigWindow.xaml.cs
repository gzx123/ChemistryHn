﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chemistry.Models;
using MahApps.Metro.Controls.Dialogs;

namespace Chemistry.Views
{
    /// <summary>
    /// ConfigWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ConfigWindow : MahApps.Metro.Controls.MetroWindow
    {
        public ConfigWindow()
        {
            InitializeComponent();

            Unloaded += ConfigWindow_Unloaded;

            Messenger.Default.Register<object>(this, MessageToken.SaveConfigOver,async (i) => {
               await this.ShowMessageAsync("提示","保存成功");
                this.Close();

            });


            Messenger.Default.Register<string>(this, MessageToken.SaveConfigError,async (i) => {
               await this.ShowMessageAsync("提示", i);
            });
        }

        void ConfigWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

         

         
    }
}
