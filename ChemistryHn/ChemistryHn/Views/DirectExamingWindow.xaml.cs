﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;

namespace Chemistry.Views
{
    /// <summary>
    /// DirectExamingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DirectExamingWindow : MetroWindow
    {
        public DirectExamingWindow()
        {
            InitializeComponent();
            var width = SystemParameters.PrimaryScreenWidth;
            Left = width - this.Width;
            Top = 0;

            Messenger.Default.Register<string>(this, MessageToken.CheckDetail, (i) =>
            {
                Messenger.Default.Send<object>("", MessageToken.HidenDirectExamingWindow);
            });


            Messenger.Default.Register<object>(this, MessageToken.HidenDirectExamingWindow, (i) =>
            {
                Messenger.Default.Unregister(this);
                this.Hide();
            });
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send<object>("", MessageToken.HidenDirectExamingWindow);
        }
    }
}
