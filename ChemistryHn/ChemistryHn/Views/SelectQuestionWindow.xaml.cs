﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Chemistry.Views
{
    /// <summary>
    /// SelectQuestionWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SelectQuestionWindow : MahApps.Metro.Controls.MetroWindow
    {

        public SelectQuestionWindow(string classname)
        {
            InitializeComponent();
            Messenger.Default.Send<string>(classname, MessageToken.SelectQuestion);

            Messenger.Default.Register<object>(this, MessageToken.ShowInputExamineeWindow, (i) =>
            {
                this.Close();

            });

            Unloaded += SelectQuestionWindow_Unloaded;
        }

        void SelectQuestionWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
