﻿using Chemistry.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chemistry.Views
{
    using MahApps.Metro.Controls;

    /// <summary>
    /// ExamingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ExamingWindow : MetroWindow
    {
        public ExamingWindow()
        {
            InitializeComponent();
            var width = SystemParameters.PrimaryScreenWidth;
            Left = width - this.Width;
            Top = 0;

           // Unloaded += ExamingWindow_Unloaded;

            Messenger.Default.Register<string>(this, MessageToken.CheckDetail, (i) =>
            {
                Messenger.Default.Send<object>("", MessageToken.HidenExamingWindow);
            });


            Messenger.Default.Register<List<ExaminationPoint>>(this, MessageToken.CheckQuestion,
                (i) =>
                    {
                        CheckQuestionWindow win = new CheckQuestionWindow();
                        win.ShowDialog();
                    });

            Messenger.Default.Register<object>(this, MessageToken.HidenExamingWindow, (i) =>
            {
                Messenger.Default.Unregister(this);
                this.Hide();
            });
            
        }

        private void PgChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnleak_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TgChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Messenger.Default.Send<object>("", MessageToken.HidenExamingWindow);
        }


        private void ExamingWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (buttonpage.datas.Items.Count > 0)
            {
                this.Height = buttonpage.ActualHeight + 360;
            }
            else
            {
                this.Height = 360;
            }
                
            Console.WriteLine(buttonpage.ActualHeight);
            //==0:  height:460
        }
    }
}
