﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chemistry.Views.ExamViews
{
    using System.ComponentModel;
    using System.Windows.Media.Animation;

    using Chemistry.Models;
    using Chemistry.Tools;

    using GalaSoft.MvvmLight.Messaging;

    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;

    /// <summary>
    /// InputNumberWindow.xaml 的交互逻辑
    /// </summary>
    public partial class InputNumberWindow : MetroWindow
    {

        private bool isBeginExaming = false;

        public InputNumberWindow()
        {
            InitializeComponent();
            InitMessage();
            this.Unloaded += ExamWindow_Unloaded;
        }

        void ExamWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        private DoubleAnimation GetHideAnimation()
        {
            DoubleAnimation hideWidthAnimation = new DoubleAnimation(
                             this.Width,
                             0,
                             new Duration(TimeSpan.FromSeconds(0.3)));
            return hideWidthAnimation;
        }

        private DoubleAnimation GetShowAnimation()
        {
            DoubleAnimation showWidthAnimation = new DoubleAnimation(
                                   0,
                                   this.Width,
                                   new Duration(TimeSpan.FromSeconds(0.3)));
            return showWidthAnimation;
        }

        private void InitMessage()
        {

            Messenger.Default.Register<Examinee>(
                this,
                MessageToken.K3ShowConfirm,
                async (i) =>
                    {
                        grid_input.BeginAnimation(WidthProperty, GetHideAnimation());
                        await Task.Delay(500);
                        grid_info.BeginAnimation(WidthProperty, GetShowAnimation());
                    });

            Messenger.Default.Register<K3ExamMessage>(this, MessageToken.K3BeginExaming,
                (i) =>
                    {
                        isBeginExaming = true;
                        QuestionWindow win = new QuestionWindow();
                        win.MainWindow = this.MainWindow;
                        win.InputWindow = this;
                        win.Show();
                        this.Hide();
                    });

            Messenger.Default.Register<object>(
                this,
                MessageToken.K3ExamingOver,
                async (o) =>
                    {
                        grid_input.BeginAnimation(WidthProperty, GetShowAnimation());
                        await Task.Delay(500);
                        TxtNumber.Focus();
                        grid_info.BeginAnimation(WidthProperty, GetHideAnimation());
                    });

            Messenger.Default.Send<DialogCoordinator>(DialogCoordinator.Instance, MessageToken.K3InputLoad);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Messenger.Default.Send<string>(TxtNumber.Text.Trim(), MessageToken.k3InputNumberOver);
            }
        }

        public MainWindow MainWindow { get; set; }

        private void InputNumberWindow_OnClosing(object sender, CancelEventArgs e)
        {
           // if(!isBeginExaming)
                MainWindow.Show();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            DoubleAnimation hideWidthAnimation = new DoubleAnimation(
                           this.Width,
                           0,
                           new Duration(TimeSpan.FromSeconds(0.3)));
            DoubleAnimation showWidthAnimation = new DoubleAnimation(
                0,
                this.Width,
                new Duration(TimeSpan.FromSeconds(0.3)));

            grid_info.BeginAnimation(WidthProperty, hideWidthAnimation);
            grid_input.BeginAnimation(WidthProperty, showWidthAnimation);
        }

        private void TxtNumber_OnLoaded(object sender, RoutedEventArgs e)
        {
            TxtNumber.Focus();
        }
    }
}
