﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chemistry.Views.ExamViews
{
    using System.ComponentModel;

    using Chemistry.Models;

    using GalaSoft.MvvmLight.Messaging;

    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;

    /// <summary>
    /// QuestionWindow.xaml 的交互逻辑
    /// </summary>
    public partial class QuestionWindow : MetroWindow
    {
        public QuestionWindow()
        {
            InitializeComponent();
            InitMessage();
            this.Unloaded += ExamWindow_Unloaded;
        }

        void ExamWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        private void InitMessage()
        {
            Messenger.Default.Register<object>(this, MessageToken.K3ExamingOver,
                (o) =>
                {
                    this.InputWindow.Show();
                    this.Close();
                });
        }

        //文字题选项
        private void Image_OnMouseDown(object sender, MouseButtonEventArgs e)
        {           
            Image image = sender as Image;
            string selection = image.Tag.ToString();
            SendQuestionOverAnswer sa = new SendQuestionOverAnswer() { QuestionType = EnumQuestionType.Text, SelectAnswer = selection };
            Messenger.Default.Send<SendQuestionOverAnswer>(sa, MessageToken.K3GetOptions);
        }

        //图片题
        private void Image_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<SendQuestionOverAnswer> selectedoptions = new List<SendQuestionOverAnswer>();
            ListBox listbox = sender as ListBox;
            foreach (var item in listbox.SelectedItems)
            {
                OptionsInfo oinfo = item as OptionsInfo;
                selectedoptions.Add(new SendQuestionOverAnswer(){ QuestionType = EnumQuestionType.Image, SelectAnswer = oinfo.Otitle});
            }
            Messenger.Default.Send<List<SendQuestionOverAnswer>>(selectedoptions, MessageToken.K3GetOptions);
        }

        private void Video_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<SendQuestionOverAnswer> selectedoptions = new List<SendQuestionOverAnswer>();
            ListBox listbox = sender as ListBox;
            foreach (var item in listbox.SelectedItems)
            {
                OptionsInfo oinfo = item as OptionsInfo;
                selectedoptions.Add(new SendQuestionOverAnswer() { QuestionType = EnumQuestionType.Video, SelectAnswer = oinfo.Otitle });
            }
            Messenger.Default.Send<List<SendQuestionOverAnswer>>(selectedoptions, MessageToken.K3GetOptions);
        }
        bool isPlaying = true;
        private void mediaPlayer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (isPlaying)
            {
                mediaPlayer.Pause();
                isPlaying = false;
            }
            else
            {
                isPlaying = true;
                mediaPlayer.Play();
            }
        }

        private void MediaPlayer_OnLoaded(object sender, RoutedEventArgs e)
        {
            //mediaPlayer.Play();
        }

        private void MediaPlayer_OnMediaEnded(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Stop();
            mediaPlayer.Play();
        }

        public MainWindow MainWindow { get; set; }

        private void QuestionWindow_OnClosing(object sender, CancelEventArgs e)
        {
            //this.MainWindow.Show();
        }

        public InputNumberWindow InputWindow { get; set; }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FlipView fv = sender as FlipView;
            if (fv.SelectedIndex == 2)
            {
                mediaPlayer.Play();
            }
            else
            {
                mediaPlayer.Stop();
            }
        }
    }
}
