﻿
using Chemistry.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chemistry.Models;

namespace Chemistry.Views
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : MahApps.Metro.Controls.MetroWindow
    {
        public Login()
        {
            InitializeComponent();
            this.DataContext = new LoginViewModel(DialogCoordinator.Instance);
            Messenger.Default.Register<object>(this, MessageToken.ShowMainWindow, (i) => {
                MainWindow win = new MainWindow();
                win.Show();
                this.Close();
            });
            //var width =SystemParameters.PrimaryScreenWidth;
            //Left = width - this.Width;
            //Top = 0;.

            if (Global.NetMode == NetModel.Stand)
            {
                MainWindow win = new MainWindow();
                win.Show();
                this.Close();
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {

        }
    }
}
