﻿
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    /// <summary>
    /// 考试信息 前台显示用
    /// </summary>
    public class ExamInfo : ObservableObject
    {

        public ExamInfo()
        {
            this.Permission = new Permission();
        }
        public int Index { get; set; }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged("Name"); }
        }

        private string cardId;
        /// <summary>
        /// 身份证号
        /// </summary>
        public string CardId
        {
            get { return cardId; }
            set { cardId = value; RaisePropertyChanged("CardId"); }
        }

        private string _ticket;
        /// <summary>
        /// 准考证号
        /// </summary>
        public string TicketId
        {
            get { return _ticket; }
            set { _ticket = value; RaisePropertyChanged("TicketId"); }
        }


        private DateTime? logindate;
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime? LoginDate
        {
            get { return logindate; }
            set { logindate = value; RaisePropertyChanged("LoginDate"); }
        }


        private ExamState _state;
        /// <summary>
        /// 考试状态
        /// </summary>
        public ExamState ExamState
        {
            get { return _state; }
            set { _state = value; RaisePropertyChanged("ExamState"); }
        }

        /// <summary>
        /// 考生
        /// </summary>
        public Examinee Examinee { get; set; }

        //考生字段信息
        private int score;

        public int Score
        {
            get { return score; }
            set { score = value; RaisePropertyChanged("Score"); }
        }
        public Permission Permission { get; set; }


        //public Chemistry.WorkFlow.WorkFlowBase WorkFlow { get; set; }

        //public List<ExaminationPoint> Points { get; set; }

    }

    public class Permission : ObservableObject
    {
        //命令按钮IsEnabled
        private bool _caninput = false;
        /// <summary>
        /// 是否可以录入考生信息
        /// </summary>
        public bool CanInput
        {
            get { return _caninput; }
            set { _caninput = value; RaisePropertyChanged("CanInput"); }
        }

        private bool _candelete;
        /// <summary>
        /// 是否可以删除
        /// </summary>
        public bool CanDelete
        {
            get { return _candelete; }
            set { _candelete = value; RaisePropertyChanged("CanDelete"); }
        }

        private bool _canCommit;
        /// <summary>
        /// 是否可以交卷
        /// </summary>
        public bool CanCommit
        {
            get { return _canCommit; }
            set { _canCommit = value; RaisePropertyChanged("CanCommit"); }
        }

        private bool _canPrint;
        /// <summary>
        /// 是否可以打印
        /// </summary>
        public bool CanPrint
        {
            get { return _canPrint; }
            set { _canPrint = value; RaisePropertyChanged("CanPrint"); }
        }

        private bool _canresit;
        /// <summary>
        /// 是否可以补考
        /// </summary>
        public bool CanResit
        {
            get { return _canresit; }
            set { _canresit = value; RaisePropertyChanged("CanResit"); }
        }

        private bool _canlink=true;
        /// <summary>
        /// 是否可以连接设备
        /// </summary>
        public bool CanLink
        {
            get { return _canlink; }
            set { _canlink = value; RaisePropertyChanged("CanLink"); }
        }

        private bool _canBegin;
        /// <summary>
        /// 是否可以开始考试
        /// </summary>
        public bool CanBeginExam
        {
            get { return _canBegin; }
            set { _canBegin = value; RaisePropertyChanged("CanBeginExam"); }
        }


        private bool _canCheckState;
        /// <summary>
        /// 是否可以查看考试状态
        /// </summary>
        public bool CanCheckState
        {
            get { return _canCheckState; }
            set { _canCheckState = value; RaisePropertyChanged("CanCheckState"); }
        }

        private bool _canCheckDetail;
        /// <summary>
        /// 是否可以查看考生成绩详情
        /// </summary>
        public bool CanCheckDetail
        {
            get { return _canCheckDetail; }
            set { _canCheckDetail = value; RaisePropertyChanged("CanCheckDetail"); }
        }

        private bool _canSelectQuestion;
        /// <summary>
        /// 是否选择题目
        /// </summary>
        public bool CanSelectQuestion
        {
            get { return _canSelectQuestion; }
            set { _canSelectQuestion = value; RaisePropertyChanged("CanSelectQuestion"); }
        }

        private bool _canCheckSelected;
        /// <summary>
        /// 是否选择题目
        /// </summary>
        public bool CanCheckSelected
        {
            get { return _canCheckSelected; }
            set { _canCheckSelected = value; RaisePropertyChanged("CanCheckSelected"); }
        }

        
    }


    /// <summary>
    /// 考生类
    /// </summary>
    public class Examinee
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("姓名")]
        public string Name { get; set; }

        [DisplayName("工种")]
        public string ExamType { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        [DisplayName("准考证号")]
        public string TicketId { get; set; }

        [DisplayName("身份证号")]
        public string CardId { get; set; }

        [DisplayName("分数")]
        public float Score { get; set; }

        [DisplayName("备注")]
        public string Remark { get; set; }

        [DisplayName("分数2")]
        public float Score2 { get; set; }

        [DisplayName("总分")]
        public float TotalScore { get; set; }

        [DisplayName("考试日期")]
        public DateTime ExamDate { get; set; }

        [DisplayName("考试批次")]
        public int Batch { get; set; }

        [DisplayName("是否完成考试")]
        public bool IsExamed { get; set; }


        [DisplayName("补考次数")]
        public int ResitCount { get; set; }
        /// <summary>
        /// 补考分数
        /// </summary>
        [DisplayName("补考分数")]
        public float ResitScore { get; set; }

        public Guid Guid { get; set; }
        //绑定属性
        public string IsResit
        {
            get
            {
                return ResitCount == 0 ? "否" : "是";
            }
        }

        /// <summary>
        /// 照片
        /// </summary>
        public byte[] Pictrue { get; set; }

        /// <summary>
        /// 单位机构
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 是否泄漏
        /// </summary>
        public bool IsLeakAbnormal { get; set; }

        /// <summary>
        /// 是否温度异常
        /// </summary>
        public bool IsTemperatureAbnormal { get; set; }

        /// <summary>
        /// 是否压力异常
        /// </summary>
        public bool IsPressureAbnormal { get; set; }
    }

    /// <summary>
    /// 调节大小的按钮
    /// </summary>
    public class ButtonInformation
    {
        public string Description { get; set; }
        public string OpcName { get; set; }
        public int Value { get; set; }
    }

}
