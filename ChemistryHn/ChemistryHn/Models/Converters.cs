﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Chemistry.Models
{
    public class BoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            object obj = null;
            if (parameter != null && parameter.ToString() == "1")//color
            {
                bool b = System.Convert.ToBoolean(value);
                //Color c = Helpers.GetColor("#FFFF6262");//红色
                //if (b)
                //{
                //    c = Helpers.GetColor("#FF3CEE44");//绿色
                //}
                //obj = c;

                obj = b ? "#FF3CEE44" : "#FFFF6262";

            }
            else
            {
                string result = "否";
                bool b = System.Convert.ToBoolean(value);
                if (b)
                {
                    result = "是";
                }
                obj = result;
            }

            return obj;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color c = Helpers.GetColor("#FF6CCB6C");

            ExamState state = (ExamState)Enum.Parse(typeof(ExamState), value.ToString());
            switch (state)
            {
                case ExamState.空闲:
                case ExamState.考试完成:
                    c = Helpers.GetColor("#FF6CCB6C");//浅绿
                    break;
                case ExamState.考试中:
                    c = Helpers.GetColor("#FFEA4B43");//红色
                    break;
                case ExamState.未重置:
                    c = Helpers.GetColor("#FF494948");//黑色
                    break;
                case ExamState.未连接:
                case ExamState.断开连接:
                    c = Helpers.GetColor("#FF747474");//灰色
                    break;
            }
            Brush brush = new SolidColorBrush(c);
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }


    public class VisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = System.Convert.ToBoolean(value);
            Visibility v = b ? Visibility.Visible : Visibility.Collapsed;
            return v;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Operator oper =(Operator) Enum.Parse(typeof(Operator), value.ToString());

            switch (oper)
            {
                case Operator.大于:
                    return ">";
                    
                case Operator.小于:
                    return "<";
                default:
                    return oper;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class Helpers
    {
        public static Color GetColor(string colorstring)
        {

            if (colorstring.IndexOf("#") == 0)
            {
                colorstring = colorstring.Substring(1, colorstring.Length - 1);
            }

            byte a = (byte)System.Convert.ToInt32(colorstring.Substring(0, 2), 0x10);
            byte r = (byte)System.Convert.ToInt32(colorstring.Substring(2, 2), 0x10);
            byte g = (byte)System.Convert.ToInt32(colorstring.Substring(4, 2), 0x10);
            byte b = (byte)System.Convert.ToInt32(colorstring.Substring(6, 2), 0x10);
            return Color.FromArgb(a, r, g, b);
        }
    }
}
