﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    /// <summary>
    /// 考试状态
    /// </summary>
    public enum ExamState
    {
        空闲,
        考试中,
        考试完成,
        未重置,
        未连接,
        断开连接,
        考生已登录
    }

    public enum RunModel
    {
        练习模式,
        演示模式,
        考试模式,
    }

    /// <summary>
    /// 运算符
    /// </summary>
    public enum Operator
    {
        大于,
        小于,
        区间,
        无
    }

    public enum NetModel
    {
        /// <summary>
        /// 单机
        /// </summary>
        Stand,
        /// <summary>
        /// 网络
        /// </summary>
        Net
    }

}
