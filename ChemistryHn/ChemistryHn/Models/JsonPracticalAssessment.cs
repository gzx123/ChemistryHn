﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    public class JsonPracticalAssessmentClass
    {
        public int Id
        {
            get;
            set;
        }

        public string ClassCode
        {
            get;
            set;
        }

        public string ClassName
        {
            get;
            set;
        }

        public string ApplicantCompanyCode
        {
            get;
            set;
        }

        public string Applicant
        {
            get;
            set;
        }

        public DateTime? StartTimePlaned
        {
            get;
            set;
        }

        public DateTime? EndTimePlaned
        {
            get;
            set;
        }

        public DateTime? PracticalAssessmentTime
        {
            get;
            set;
        }

        public DateTime? PracticalAssessmentStartTime
        {
            get;
            set;
        }
        public DateTime? PracticalAssessmentEndTime
        {
            get;
            set;
        }

        public string PracticalAssessmentAddress
        {
            get;
            set;
        }
    }

    public class JsonPracticalAssessmentResult
    {
        /// <summary>
        /// 班期编号
        /// </summary>
        public string ClassCode
        {
            get;
            set;
        }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IDNumber
        {
            get;
            set;
        }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamNumber
        {
            get;
            set;
        }

        /// <summary>
        /// 考试类型：1、理论；2、实践
        /// </summary>
        public int ExamCategory
        {
            get;
            set;
        }

        /// <summary>
        /// 科目三总成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject3Score
        {
            get;
            set;
        }
        /// <summary>
        /// 科目三通用单元1成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject3Universal1Score
        {
            get;
            set;
        }
        /// <summary>
        /// 科目三通用单元2成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject3Universal2Score
        {
            get;
            set;
        }
        /// <summary>
        /// 科目三特殊单元成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject3SpecialScore
        {
            get;
            set;
        }

        /// <summary>
        /// 科目四成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject4Score
        {
            get;
            set;
        }

        /// <summary>
        /// 科目四通用单元1成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject4Universal1Score
        {
            get;
            set;
        }
        /// <summary>
        /// 科目四通用单元2成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject4Universal2Score
        {
            get;
            set;
        }
        /// <summary>
        /// 科目四特殊单元成绩：若为null表示考生缺考
        /// </summary>
        public decimal? Subject4SpecialScore
        {
            get;
            set;
        }

        /// <summary>
        /// 是否补考：0、初试；1、补考
        /// </summary>
        public int IsSupplementary
        {
            get;
            set;
        }
    }

    public class JsonPracticalAssessmentStudent
    {
        public int Id
        {
            get;
            set;
        }
        public string ClassCode
        {
            get;
            set;
        }
        public string StudentName
        {
            get;
            set;
        }
        public string StudentIDNumber
        {
            get;
            set;
        }
        public string StudentDegree
        {
            get;
            set;
        }
        public string StudentCompany
        {
            get;
            set;
        }
        public string StudentPhone
        {
            get;
            set;
        }
        public string StudentAdress
        {
            get;
            set;
        }
        public string StudentCraftCode
        {
            get;
            set;
        }
        public string StudentCraftName
        {
            get;
            set;
        }
        public string StudentPhotoUrl
        {
            get;
            set;
        }
        public string StudentExamNumber
        {
            get;
            set;
        }
        public string StudentSex
        {
            get;
            set;
        }

        public decimal? Item1Score
        {
            get;
            set;
        }
        public decimal? Item2Score
        {
            get;
            set;
        }
    }
}
