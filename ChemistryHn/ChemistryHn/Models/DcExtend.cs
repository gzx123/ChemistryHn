﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    public static class DcExtend
    {
        /// <summary>
        /// 开始流动、闪烁
        /// </summary>
        /// <param name="opcitem"></param>
        public static void StartFlow(this OPCItem opcitem)
        {
            
            try
            {
                if (opcitem != null)
                opcitem.Write(1);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 停止流动、闪烁
        /// </summary>
        /// <param name="opcitem"></param>
        public static void StopFlow(this OPCItem opcitem)
        {
            
            try
            {
                if (opcitem != null)
                opcitem.Write(0);
            }
            catch (Exception)
            {
                
            }
            
        }

        /// <summary>
        /// 打开阀门
        /// </summary>
        /// <param name="opcitem"></param>
        public static void Open(this OPCItem opcitem)
        {
            try
            {
                if (opcitem != null)
                    opcitem.Write(1);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 关闭阀门
        /// </summary>
        /// <param name="opcitem"></param>
        public static void Close(this OPCItem opcitem)
        {
            try
            {
                if (opcitem != null)
                    opcitem.Write(0);
            }
            catch (Exception)
            {

            }
        }
    }
}
