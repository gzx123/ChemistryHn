﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    /// <summary>
    /// 返回值类
    /// </summary>
    public class ResultData
    {
        public int status { get; set; }
        public string session { get; set; }
    }

    /// <summary>
    /// 分数类  名-值 对
    /// </summary>
    public class Score
    {
        public string name { get; set; }
        public decimal value { get; set; }

    }

    /// <summary>
    /// 上传用的分数实体类
    /// </summary>
    public class ScoreData
    {
        public string session { get; set; }
        public string nr { get; set; }

        public IList<Score> items { get; set; }

    }

    public class ExamineeInfoApi
    {
        public int status { get; set; }

        public ExamineeInfoRemote examinee { get; set; }

        public List<string> subject { get; set; }
        
    }

    /// <summary>
    /// 服务器端考生信息
    /// </summary>
    public class ExamineeInfoRemote
    {
        public int autoId { get; set; }
        public string name { get; set; }

        public string WTRoot { get; set; }

        public string WTAdded { get; set; }

        public string school { get; set; }

        public string photo { get; set; }
    }


    /// <summary>
    /// Workflow远程数据结构
    /// </summary>
    public class WorkflowApiModel
    {
        public int status { get; set; }
        public List<Workflow> Workflows { get; set; }
    }

    /// <summary>
    /// 考点apiModel
    /// </summary>
    public class ExamPointApiModel
    {
        public int status { get; set; }
        public List<ExaminationPoint> points { get; set; }
    }

    public class DetaiApiModel
    {
        public int status { get; set; }
        public List<Detail> details { get; set; }
    }

    public class AbnormalApiModel
    {
        public int status { get; set; }
        public List<Abnormal> abnormals { get; set; }
    }

    public class ChemistryApiReuslt
    {
        public int status { get; set; }
    }
}
