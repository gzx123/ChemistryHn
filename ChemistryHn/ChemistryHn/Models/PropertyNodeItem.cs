﻿
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    public class PropertyNodeItem : ObservableObject
    {
        public PropertyNodeItem()
        {
            this.Children = new List<PropertyNodeItem>();
        }
        public string  Name { get; set; }

        public object Tag { get; set; }

        public List<PropertyNodeItem> Children { get; set; }
    }
}
