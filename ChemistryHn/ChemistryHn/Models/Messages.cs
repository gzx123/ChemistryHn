﻿using Chemistry.WorkFlow;
using OPCAutomation;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    using Chemistry.Tools;

    /// <summary>
    /// 消息Token
    /// </summary>
    public enum MessageToken
    {
        ShowMainWindow,
        SaveConfigOver,
        SaveConfigError,
        SelectFlow,
        CheckExaminee,
        CheckExamineeOver,
        ShowInputExamineeWindow,
        ShowConfirmWindow,
        InputNumberOver,
        ShowExamingWindow,
        SelectFlowForPoint,
        SelectQuestion,
        ShowExamWindow,
        ShowSelectQuestionWindow,
        ShowConfirm,
        ShowInputExaminee,
        ShowInputNumber,
        ShowConfirId,
        ShowExamMessage,
        BeginExaming,
        HidenExamingWindow,
        CheckDetail,
        IsExaming,
        CheckSelectedQuestion,
        GetButtonConfig,
        ShowRegulationWindow,
        StopExaming,
        SendButtonValue,
        k3InputNumberOver,
        K3ShowConfirm,
        K3BeginExaming,
        K3GetOptions,
        K3ExamingOver,
        K3InputLoad,
        ShowExamLoadMessage,
        CloseExamLoadMessage,
        CheckQuestion,
        ShowDirectExamingWindow,
        HidenDirectExamingWindow
    }

    public class ExamInfoMessage
    {
        public ExamInfo ExamInfo { get; set; }
        public bool IsFirstShow { get; set; }
        public bool IsStep { get; set; }

        public List<ButtonInformation> ButtonInformations { get; set; }

        public List<ExaminationPoint> Points { get; set; }
        /// <summary>
        /// 当前考核的工艺流程
        /// </summary>
        public WorkFlowBase WorkFlow { get; set; }
    }


    /// <summary>
    /// k3考试消息
    /// </summary>
    public class K3ExamMessage
    {
        public List<QuestionInfo> QuestionInfoes { get; set; }

        public List<OptionsInfo> OptionsInfoes { get; set; }

        public Examinee Examinee { get; set; }
    }

    /// <summary>
    /// 已答题
    /// </summary>
    public class SendQuestionOverAnswer
    {
       
        public int QuestionId { get; set; }
        public EnumQuestionType QuestionType { get; set; }
        public string SelectAnswer { get; set; }

        public override bool Equals(object obj)
        {
            SendQuestionOverAnswer other = obj as SendQuestionOverAnswer;
            return this.QuestionId == other.QuestionId && this.SelectAnswer == other.SelectAnswer && this.QuestionType == other.QuestionType;
            //return base.Equals(obj);
        }
    }

    /// <summary>
    /// 选中项
    /// </summary>
    public class SendAnswer
    {
        public EnumQuestionType QuestionType { get; set; }
        public string SelectAnswer { get; set; }
    }
}
