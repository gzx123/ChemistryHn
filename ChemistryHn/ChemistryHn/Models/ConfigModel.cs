﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chemistry.Models
{
    [Serializable]
    public class ConfigModel : NotificationObject
    {
        private string _ip="127.0.0.1";

        public string IP
        {
            get { return _ip; }
            set { _ip = value; RaisePropertyChanged("IP"); }
        }

        private string _opcservername;

        public string OPCServerName
        {
            get { return _opcservername; }
            set { _opcservername = value; RaisePropertyChanged("OPCServerName"); }
        }

        public string Name { get; set; }
        public int BaudRate { get; set; }
        public int DataBit { get; set; }
        public StopBits StopBit { get; set; }
        public Parity Parity { get; set; }



        public string WorkflowClassName { get; set; }

        public string WorkflowChineseName { get; set; }
         
        private string _printpath;

        public string PrintPath
        {
            get { return _printpath; }
            set { _printpath = value; RaisePropertyChanged("PrintPath"); }
        }

        public bool IsInner { get; set; }

        
    }

    [Serializable]
   public class NotificationObject : INotifyPropertyChanged
    {
        [field:NonSerialized]

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
