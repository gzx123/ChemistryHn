using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leafing.Data.Definition;

namespace DbEntryClassLibrary.Models
{
    public class User : DbObjectModel<User>
    {
        public string Name { get; set; }
    }
}
