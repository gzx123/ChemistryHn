﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leafing.Data.Definition;

namespace DbEntryClassLibrary.Models
{
    [DbTable("WorkFlows")]
    public class WorkFlow : DbObjectModel<WorkFlow>
    {
        public string Name { get; set; }
        public string ClassName { get; set; }
    }
    [DbTable("Details")]
    public class Detail : DbObjectModel<Detail>
    {
        public string Step { get; set; }
        public int Score { get; set; }
        public int WorkFlowId { get; set; }
    }

    [DbTable("Abnormals")]
    public class Abnormal : DbObjectModel<Abnormal>
    {
        public string Description { get; set; }
        public int Rate { get; set; }
        public int Value { get; set; }
        public int Operator { get; set; }
        public int WorkFlowId { get; set; }
    }
}
